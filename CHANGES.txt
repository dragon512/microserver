v1.0.0  Dec. 3, 2018 -- Initial release

v1.0.1  Jan. 4, 2019 -- Fixed bugs where SSL server was starting on the nonSSL server's port and fixed up the read hook logic